from src.utils import *

if __name__ == '__main__':
    data = read_file("./gl-latlong-1km-landcover.bsq/gl-latlong-1km-landcover.bsq","uint8")
    lat, long = get_user_input()
    define_land_and_water(lat,long,data)
